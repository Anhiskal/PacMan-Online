mport zmq
import sys
import json

from Jugador import Jugador

informacionDeLosJugadore = []
listaDeJugadores = {}

listaDeGalletas = []

nombreDelServidor = "Servidor"

mensajeUnirmeAlJuego = 0
mensajeMovimiento = 1
mensajeCambiarDeEstadoElJuego = 2
mensajeUnJugadorMurio = 3

NumJugadores = 5
estadoDelJuego = 0


posInicialFantasmas = [11,2]
posInicialPacmans = [2,14]
posicionInicial = []
mensajeAEnviar = {}
informacionDelJugador = {}
soyElServidor = "Servidor"

def crearMensaje(tipoDeMensaje, informacionAEnviar):
	mensaje = {}
	mensaje["TipoDeMensaje"] = tipoDeMensaje
	mensaje["Mensaje"] = informacionAEnviar
	msg = json.dumps(mensaje)

	return msg

def retornarInformacionDelJugador(jugador):
	info = {}

	info["Nombre"] = jugador.Nombre
	info["PosicionActual"] = [jugador.posXActual, jugador.posYActual]
	info["TipoDeJugador"] = jugador.tipoJugador
	info["CantidadDeVidas"] = jugador.vidas
	info["Puntaje"] = jugador.puntaje
	info["EstoyMuerto"] = jugador.estoyMuerto

	return info

def enviarMensajeATodosLosJugadores(socket, informacionAEnviar):	

	for jugador in informacionDeLosJugadore:
		socket.send_multipart([bytes(listaDeJugadores[jugador.Nombre], 'ascii'), bytes(mensaje, 'ascii')])

def indiceDelJugador(nombre):
	indice = 0
	for jugador in informacionDeLosJugadore:
		if jugador.Nombre == nombre:
			break;
		else :
			indice += 1
	return indice


def decodificarMensaje(mensajeRecibido, socket, ident):
	#se transforma el mensaje de byte a json
	datosRecibidos = json.loads(mensajeRecibido)

	print(datosRecibidos["TipoDeMensaje"])
	tipoDeMensaje = datosRecibidos["TipoDeMensaje"]
	mensaje = datosRecibidos["Mensaje"]

	#Un jugador se unio a la partida
	if tipoDeMensaje == 1:
		print("Un jugador quiere unirse")
		nuevoJugador = Jugador()

		nombreJugador = mensaje["Nombre"]
		listaDeJugadores[nombreJugador] = ident
		nuevoJugador.iniciar(nombreJugador)


		if len(listaDeJugadores) == 1:
			#Es el primer jugador en unirse
			tipoJugador = 3
			posicionInicial = posInicialFantasmas

			nuevoJugador.inicializarJuego(tipoJugador, posicionInicial)
			informacionDeLosJugadore.addend(nuevoJugador)

			info = retornarInformacionDelJugador(nuevoJugador)
			mensaje = crearMensaje(0, info)
			enviarMensajeATodosLosJugadores(socket, mensaje)
			#Envia un mensaje al jugador


		else :
			#Se agrego un jugador que no es el primero
			if len(listaDeJugadores) % 5 == 0 :
				tipoJugador = 4
				posicionInicial = posInicialPacmans

			if len(listaDeJugadores) % 5 != 0 :
				tipoJugador = 3
				posicionInicial = posInicialFantasmas

			nuevoJugador.inicializarJuego(tipoJugador, posicionInicial)

			#Le envio un mensaje a todos los demas jugadores que uno nuevo se integro al juego.
			informacionDelNuevoJugador = retornarInformacionDelJugador(nuevoJugador)
			#Como el jugador nuevo no se a agreagado a la lista de informacion de los jugadores no se le enviara el mensaje a el
			nuevoMensaje = crearMensaje(1, informacionDelNuevoJugador)
			enviarMensajeATodosLosJugadores(socket, nuevoMensaje)

			#Se tomara la informacion de cada uno de los jugadores antiguos y se formara en un mensaje con este
			informacionDeTodosLosJugadores = []
			for jugador in informacionDeLosJugadore:
				informacionDeEsteJugador = retornarInformacionDelJugador(jugador)
				informacionDeTodosLosJugadores.append(informacionDeEsteJugador)


			#Le envio la informacion de todos los demas jugadores y la de si mismo en un mensaje
			informacionDeLosJugadore.addend(nuevoJugador)
			informacionDeTodosLosJugadores.append(informacionDelNuevoJugador)
			info = {}
			info["InformacionDeLosJugadores"] = informacionDeTodosLosJugadores
			otroMensaje = crearMensaje(0, info)
			mensaje = json.dumps(otroMensaje)
			socket.send_multipart([bytes(listaDeJugadores[nombreJugador], 'ascii'), bytes(mensaje, 'ascii')])


	#un jugador genero un movimiento
	if tipoDeMensaje == 2:
		nombreJugador = mensaje["Nombre"]
		nuevaPosicionDelJugador = mensaje["Posicion"]

		enviarMensajeATodosLosJugadores(2,mensaje)

		#Actualiza la informacion del jugador
		indiceJugador = indiceDelJugador(nombreJugador)
		informacionDeLosJugadore[indiceDelJugador].posXActual = nuevaPosicionDelJugador[0]
		informacionDeLosJugadore[indiceDelJugador].posYActual = nuevaPosicionDelJugador[1]



	#Cambio de estado
	#if tipoDeMensaje == 4:




	#un jugador Murio
	if tipoDeMensaje == 0:
		nombreDelJugadorMuerto = mensaje["Nombre"]
		posiciion = mensaje["Posicion"]
		indice = indiceDelJugador(nombreDelJugadorMuerto)

		informacionDeLosJugadore[indice].EstoyMuerto = True
		informacionDeLosJugadore[indice].posXActual = posiciion[0]
		informacionDeLosJugadore[indice].posYActual = posiciion[1]




def main():
	if len(sys.argv) != 1:
		print("Must be called with no arguments")
		exit()

	context = zmq.Context()
	socket = context.socket(zmq.ROUTER)
	socket.bind("tcp://*:4444")
	poller = zmq.Poller()
	poller.register(sys.stdin, zmq.POLLIN)
	poller.register(socket, zmq.POLLIN)

	print("Started server")

	while True:
		socks = dict(poller.poll())
		if socket in socks:
			ident, dest, msg = socket.recv_multipart()
			decodificarMensaje(msg, socket, ident)


if __name__ == '__main__':
	main()