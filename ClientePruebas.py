import pygame, sys, zmq	
from pygame.locals import *

from Jugador import Jugador
from Mapa import Mapa
from Juego import Juego

mapa = Mapa()
jugador = Jugador()
juego = Juego()

nombreDelJugadorPrueba = 'Anhiskal'

jugador.iniciar( nombreDelJugadorPrueba )
jugador.inicializarJuego(3, [1,13])

juego.agregarJugador(jugador)
otroJugador = Jugador()
otroJugador.iniciar("Nemesis")
otroJugador.inicializarJuego(4, [1,11])
#juego.mapa.agregarElementoAlMapa(4, [1,13])
juego.agregarJugador(otroJugador)
puedoRealizarElMovimiento = False
posibleMovimiento = [0,0]

nombre = "Nemesis"

dirX = 0
dirY = 0
HayUnCambio = True

pygame.init()
pantalla = pygame.display.set_mode((juego.mapa.ANCHODELMAPA*juego.mapa.DIMENCIONDELACELDADELMAPA, juego.mapa.ALTODELMAPA*juego.mapa.DIMENCIONDELACELDADELMAPA))

print (juego.jugadores[0].Nombre)
print (str(juego.jugadores[0].posXActual), str(juego.jugadores[0].posYActual))


unJugadorSeUnio = True
informacionRecibida ={"TipoDeAcccion" : 1, "informacion" : {"Nombre" : "Anhiskal", "posXActual" : 1, "posYActual": 12, "TipoJugador" : 4}}

unirmeAlJuego = True

print(juego.jugadores[0].tipoJugador)
print(juego.jugadores[1].tipoJugador)

while True:
	#obtienen todos los eventos del usuario
	for evento in pygame.event.get():
		if evento.type == QUIT:
			pygame.quit()
			sys.exit()
		if evento.type == pygame.KEYDOWN:
			if evento.key == K_LEFT:
				dirX = -1
				
			elif evento.key == K_RIGHT:
				dirX = 1
				
			elif evento.key == K_UP:
				dirY = -1
								
			elif evento.key == K_DOWN:
				dirY = 1
			
			estoy ,indicePropio = juego.elJugadorExisteEnElJuego(nombre)
			if juego.jugadores[indicePropio].estoyMuerto == False:
				puedoRealizarElMovimiento, posibleMovimiento = juego.intentarHacerMovimiento([dirX, dirY],nombre)
				dirX = 0
				dirY = 0
	
	#Se envian los mensajes al servidor
	
	#Enviar movimiento al servidor
	if puedoRealizarElMovimiento == True:
		juego.generarUnMovimiento(nombre,posibleMovimiento)
		#print (str(juego.jugadores[0].posXActual), str(juego.jugadores[0].posYActual))
		posibleMovimiento = [0,0]
		HayUnCambio = True
		puedoRealizarElMovimiento = False
	
	
	#En esta parte se resiven los mensajes que envia el servidor
	
	#Si el server agrega a otro jugador
	"""if unJugadorSeUnio == True : 
		nuevoJugador = Jugador()
		nuevoJugador.iniciar(informacionRecibida["informacion"]["Nombre"])
		nuevoJugador.inicializarJuego(informacionRecibida["informacion"]["TipoJugador"], [informacionRecibida["informacion"]["posXActual"],informacionRecibida["informacion"]["posYActual"]])
		juego.agregarJugador(nuevoJugador)
		unJugadorSeUnio = False"""
		
	#if unirmeAlJuego == True:	
		
	
	if HayUnCambio == True:
		for fila in range(juego.mapa.ALTODELMAPA):
			for columna in range(juego.mapa.ANCHODELMAPA):
				pygame.draw.rect(pantalla, juego.mapa.colores[juego.mapa.mapaTile[fila][columna]],(columna*juego.mapa.DIMENCIONDELACELDADELMAPA,fila*juego.mapa.DIMENCIONDELACELDADELMAPA,juego.mapa.DIMENCIONDELACELDADELMAPA,juego.mapa.DIMENCIONDELACELDADELMAPA))
				HayUnCambio = False
	pygame.display.update()