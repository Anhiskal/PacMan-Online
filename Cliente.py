import pygame, sys, zmq, json
from pygame.locals import *

from Jugador import Jugador
from Mapa import Mapa
from Juego import Juego

mapa = Mapa()
jugador = Jugador()
jugadores = []
juego = Juego()
meConecteAlJuego = False
#puedoRealizarElMovimiento = False
destino = "Servidor"
estoyConectadoAlJuego = False
hayUnCaambio = False
nombreDeEsteJugador = ""

def crearMensaje(tipoDeMensaje, informacionAEnviar):
	mensaje = {}
	mensaje["TipoDeMensaje"] = tipoDeMensaje
	mensaje["Mensaje"] = informacionAEnviar

	enviar = json.dumps(mensaje)

	return enviar

def generarJugador(datosDelJugador):
	jugador = Jugador()
	jugador.Nombre = datosDelJugador["Nombre"]
	jugador.tipoJugador = datosDelJugador["TipoDeJugador"]
	posicionDelJugador = datosDelJugador["PosicionActual"]
	jugador.posXActual = posicionDelJugador[0]
	jugador.posYActual = posicionDelJugador[1]
	jugador.Puntaje = datosDelJugador["Puntaje"]
	jugador.estoyMuerto = datosDelJugador["EstoyMuerto"]

	return jugador

def reubicarMiJugador(nombreDelJugador):
	juego.reubicarJugador(nombreDelJugador)


def decodificarMensaje(mensajeRecibido):
	datosRecibidos = json.loads(mensajeRecibido)

	tipoDelMensaje = datosRecibidos["TipoDeMensaje"]
	mensaje = datosRecibidos["Mensaje"]

	#Un jugador se unio a la partida
	if tipoDelMensaje == 1:
		informacionDelNuevoJugador = mensaje["InformacionDelNuevoJugador"]
		nuevoJugador = generarJugador(informacionDelJugador)
		jugadores.append(nuevoJugador)

		estoyConectadoAlJuego = True


	#un jugador genero un movimiento
	if tipoDelMensaje == 2:
		posicionDelJugador = mensaje["posicionDelJugador"]
		nombreDelJugadorQueSeMovio = mensaje["NombreDelJugador"]
		juego.generarUnMovimiento(nombreDelJugadorQueSeMovio, posicionDelJugador)



	#Cambio de estado
	if tipoDelMensaje == 4:
		estadoDelJuego = mensaje["EstadoDelJuego"]
		juego.cambiarElEstadoDelJuego(estadoDelJuego)



	#Unirme a el juego
	if tipoDelMensaje == 0:
		informacionDeLosJugadores = mensaje["InformacionDeLosJugadores"]
		for informacionDelJugador in informacionDeLosJugadores:
			nuevoJugador = generarJugador(informacionDelJugador)
			jugadores.append(nuevoJugador)

		estadoDelJuego = mensaje["EstadoDelJuego"]
		juego.cambiarElEstadoDelJuego(estadoDelJuego)
		reubicarMiJugador()

		estoyConectadoAlJuego = True

	hayUnCaambio = True


#def unirmeAlServidor(nombreDelJugador):

pygame.init()
pantalla = pygame.display.set_mode((juego.mapa.ANCHODELMAPA*juego.mapa.DIMENCIONDELACELDADELMAPA, juego.mapa.ALTODELMAPA*juego.mapa.DIMENCIONDELACELDADELMAPA))


def main():
	if len(sys.argv) != 2:
		print("Must be called with an identity")
		exit()
	context = zmq.Context()
	socket = context.socket(zmq.DEALER)
	identity = sys.argv[1].encode('ascii')
	nombreDeEsteJugador = str(sys.argv[1].encode('ascii'))
	print (nombreDeEsteJugador)
	socket.identity = identity
	socket.connect("tcp://localhost:4444")
	print("Started client with id {}".format(identity))
	poller = zmq.Poller()
	poller.register(sys.stdin, zmq.POLLIN)
	poller.register(socket, zmq.POLLIN)

	mensajeDeIngreso = {}
	mensajeDeIngreso["TipoDeMensaje"] = 0
	mensajeDeIngreso["Mensaje"] = nombreDeEsteJugador
	puedoRealizarElMovimiento = False
	info = json.dumps(mensajeDeIngreso)
	socket.send_multipart([bytes(destino, 'ascii'), bytes(info, 'ascii')])


	while True:
		socks = dict(poller.poll())
		if socket in socks:
			#Si resivo una mensaje del servidor
			ident, dest, msg = socket.recv_multipart()
			decodificarMensaje(msg)

		#print ("Otras cosas asdasda")
		if estoyConectadoAlJuego == True:
			for evento in pygame.event.get():
				if evento.type == QUIT:
					pygame.quit()
					sys.exit()

				if evento.type == pygame.KEYDOWN:
					#print ("asdsadasd")
					if evento.key == K_LEFT:
						dirX = -1
						#puedoRealizarElMovimiento = True

					elif evento.key == K_RIGHT:
						dirX = 1
						#puedoRealizarElMovimiento = True

					elif evento.key == K_UP:
						dirY = -1
						#puedoRealizarElMovimiento = True

					elif evento.key == K_DOWN:
						dirY = 1
						#puedoRealizarElMovimiento = True



					if juego.jugadores[0].estoyMuerto == False:
						puedoRealizarElMovimiento, posibleMovimiento = juego.intentarHacerMovimiento([dirX, dirY], nombreDeEsteJugador)
						dirX = 0
						dirY = 0

					if juego.jugadores[0].estoyMuerto == True:
						informacion = {}
						informacion["Nombre"] = jugadores[0].Nombre
						posicion = []
						posicion[0] = jugadores[0].posXActual
						posicion[1] = jugadores[1].posYActual
						informacion["Posicion"] = posicion

					if juego.jugadores[0].estoyMuerto == True:
						mensaje = {}
						mensaje["TipoDeMensaje"]

			if  puedoRealizarElMovimiento == True:
				informacionAMandar = {}
				informacionAMandar["Nombre"] = nombreDeEsteJugador
				informacionAMandar["Posicion"] = [0,0]
				mensaje = crearMensaje(1, informacionAMandar)

				msj = json.dumps(mensaje)

				socket.send_multipart([bytes(destino,'ascii'),bytes(msj,'ascii')])
				puedoRealizarElMovimiento = False

			if HayUnCambio == True:
				for fila in range(juego.mapa.ALTODELMAPA):
					for columna in range(juego.mapa.ANCHODELMAPA):
						pygame.draw.rect(pantalla, juego.mapa.colores[juego.mapa.mapaTile[fila][columna]],(columna*juego.mapa.DIMENCIONDELACELDADELMAPA,fila*juego.mapa.DIMENCIONDELACELDADELMAPA,juego.mapa.DIMENCIONDELACELDADELMAPA,juego.mapa.DIMENCIONDELACELDADELMAPA))
						HayUnCambio = False
				pygame.display.update()







if __name__ == '__main__':
	main()