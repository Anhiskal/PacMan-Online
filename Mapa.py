class Mapa:

	DIMENCIONDELACELDADELMAPA = 20
	ANCHODELMAPA = 12
	ALTODELMAPA = 15
	
	NEGRO = (0,0,0)
	CAFE = (153, 76, 0)
	VERDE = (0,255,0)
	AZUL = (0,0,255)
	ROJO = (255,0,0)
	AMARILLO = (255,255,0)
	MORADO = (255,0,255)

	Camino = 0
	Muro = 1
	Galleta = 2
	Fantasma = 3
	Pacman = 4
	SuperGalleta = 5

	colores = {
		Pacman : AMARILLO,
		Muro : AZUL,
		Camino : NEGRO,
		Fantasma : VERDE,
		Galleta : CAFE,
		SuperGalleta : MORADO
	}

	mapaTile = [
	[Muro,Muro,Muro,Muro,Muro,Muro,Muro,Muro,Muro,Muro,Muro,Muro],
	[Muro,Camino,Galleta,Camino,Camino,Camino,Camino,Camino,Camino,Camino,Camino,Muro],
	[Muro,Camino,Muro,Muro,Muro,Camino,Muro,Muro,Muro,Muro,Camino,Muro],
	[Muro,Camino,Camino,Camino,Camino,Camino,Camino,Camino,Camino,Camino,Camino,Muro],
	[Muro,Camino,Muro,Camino,Muro,Camino,Muro,Muro,Camino,Muro,Camino,Muro],
	[Muro,Camino,Muro,Camino,Muro,Camino,Muro,Camino,Camino,Camino,Camino,Muro],
	[Muro,Camino,Camino,Camino,Muro,Camino,Camino,Camino,Muro,Muro,Camino,Muro],
	[Muro,Muro,Muro,Camino,Muro,Muro,Camino,Muro,Muro,Muro,Camino,Muro],
	[Muro,Camino,Camino,Camino,Camino,Camino,Camino,Muro,Camino,Camino,Camino,Muro],
	[Muro,Camino,Muro,Muro,Camino,Muro,Camino,Muro,Camino,Muro,Camino,Muro],
	[Muro,Camino,Camino,Camino, Camino,Camino,Camino,Camino,Camino,Camino,Camino,Muro],
	[Muro,Camino,Muro,Muro,Camino,Camino,Camino,Muro,Muro,Muro,Camino,Muro],
	[Muro,Camino,Muro,Muro,Camino,Camino,Muro,Muro,Muro,Muro,Camino,Muro],
	[Muro,Camino, Camino ,Camino,Camino,Camino,Camino,Camino,Camino,Camino,Camino,Muro],
	[Muro,Muro,Muro,Muro,Muro,Muro,Muro,Muro,Muro,Muro,Muro,Muro]
	]
	
	
	def agregarElementoAlMapa(self, tipoDeElementoAAgregar, posicion):
		self.mapaTile[posicion[1]][posicion[0]] = tipoDeElementoAAgregar	

	
	def agregarFilaAlMapa(self, fila):
		self.mapaTile.append(fila)