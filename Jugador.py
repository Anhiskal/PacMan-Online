class Jugador:
	Nombre = ''
	Puntaje = 0
	vidas = 2
	estoyMuerto = True	
	tipoJugador = 0
	#tipo = {Fantasma : 1, Pacman : 0}
	puntaje = 0
	posXActual = 0
	posYActual = 0
	posX = 0
	posY = 0
		
	
	def iniciar(self, nombre):
		self.Nombre = nombre	
		
	def inicializarJuego(self, tipoJugador, posicion):
		self.tipoJugador = tipoJugador
		self.puntaje = 0
		self.inicializarJugador(posicion)
		
	def inicializarJugador(self,posicion):
		self.actualizarPosicion(posicion)
		self.estoyMuerto = False		
				
	
	def actualizarPosicion(self, posicion):
		self.posXActual = posicion[0]
		self.posYActual = posicion[1]
		self.posX = self.posXActual
		self.posY = self.posYActual
		
	def elPersonajeMurio(self, cantidadAPerder, posInicio = []):
		self.vidas -= cantidadAPerder
		self.estoyMuerto = True
		if cantidadAPerder <= 0:			
			expulsarDelJuego()
		else:
			self.inicializarJugador(posInicio)
			
	def intentarMover(self, movimiento):
		self.posY = self.posYActual + movimiento[1]
		self.posX = self.posXActual + movimiento[0]

	
	def IntentarMoverHorizontalmente(self, movimiento):
		self.posY = self.posYActual + movimiento
		
	def IntentarMoverVerticalmente(self, movimiento):
		self.posX = self.posXActual + movimiento