from Jugador import Jugador
from Mapa import Mapa


class Juego:
	jugadores = []
	mapa = Mapa()
	estadoDelJuego = 0
	
	def agregarJugador(self, jugador):
		self.jugadores.append(jugador)
		self.mapa.agregarElementoAlMapa(jugador.tipoJugador, [jugador.posX,jugador.posY])
	
		
	
	def generarUnMovimiento(self, NombreDelJugador, movimiento):
		print ("el movimiento es : ")
		print(movimiento)
		elJugadorNoEsta, indiceDelJugador = self.elJugadorExisteEnElJuego(NombreDelJugador)		
		
		if elJugadorNoEsta == False:
			jugador = self.jugadores[indiceDelJugador]
			
			#Si el jugador es pacman
			if jugador.tipoJugador == 4:			

				self.mapa.mapaTile[jugador.posYActual][jugador.posXActual] = self.mapa.Camino
				jugador.actualizarPosicion(movimiento)
				
				if self.mapa.mapaTile[jugador.posY][jugador.posX] != self.mapa.Fantasma:
					self.mapa.mapaTile[jugador.posY][jugador.posX] = self.mapa.Pacman
				
				if self.mapa.mapaTile[jugador.posYActual][jugador.posXActual] == self.mapa.Galleta:						
					jugador.puntaje += 1				
					
				if self.mapa.mapaTile[jugador.posY][jugador.posX] == self.mapa.Fantasma:
					if self.estadoDelJuego == 0:
						self.mapa.mapaTile[jugador.posYActual][jugador.posXActual] = self.mapa.Fantasma
						jugador.estoyMuerto = True
					if self.estadoDelJuego == 1:
						self.mapa.mapaTile[jugador.posYActual][jugador.posXActual] = self.mapa.Pacman				
				
				
				
			#Si el jugador es fantasma
			if jugador.tipoJugador == 3:

				self.mapa.mapaTile[jugador.posYActual][jugador.posXActual] = self.mapa.Camino
				jugador.actualizarPosicion(movimiento)
				
				#if self.mapa.mapaTile[jugador.posY][jugador.posX] != self.mapa.Fantasma:
				#	self.mapa.mapaTile[jugador.posY][jugador.posX] = self.mapa.Pacman
				
				#if self.mapa.mapaTile[jugador.posYActual][jugador.posYActual] == self.mapa.Galleta:				
					
					
				if self.mapa.mapaTile[jugador.posY][jugador.posX] == self.mapa.Pacman:
					
					if self.estadoDelJuego == 0:			
						indiceDelPacmanMuerto = self.buscarJugadorPorPosicion([jugador.posXActual,jugador.posYActual], jugador.tipoJugador)
						print("El indice del pacman es :")
						print(indiceDelPacmanMuerto)
						self.jugadores[indiceDelPacmanMuerto].estoyMuerto = True
						self.mapa.mapaTile[jugador.posYActual][jugador.posXActual] = self.mapa.Fantasma
						
					if self.estadoDelJuego == 1:
						self.mapa.mapaTile[jugador.posYActual][jugador.posXActual] = self.mapa.Pacman
						jugador.estoyMuerto = True
				
			self.jugadores[indiceDelJugador] = jugador
		
		for juga in self.jugadores:
			if juga.estoyMuerto == False:
				self.mapa.mapaTile[juga.posYActual][juga.posXActual] = juga.tipoJugador
			
	def moverUnJugador(self, indiceDelJugador, movimiento):
		self.jugadores[indiceDelJugador].actualizarPosicion(movimiento)
	
	def elJugadorExisteEnElJuego(self, nombreDelJugador):
		indice = 0		
		elJugadorNoEsta = False
		for jugador in self.jugadores:
			if jugador.Nombre == nombreDelJugador:
				elJugadorNoEsta = False
				break;
			else :
				indice += 1
				if indice == len(self.jugadores):
					elJugadorNoEsta = True
		
		return elJugadorNoEsta, indice
		
	def intentarHacerMovimiento(self, movimiento, nombreDelJugador):
		esta, indice = self.elJugadorExisteEnElJuego(nombreDelJugador)
		jugador = self.jugadores[indice]
		jugador.intentarMover(movimiento)
		
		if self.mapa.mapaTile[jugador.posY][jugador.posX] == self.mapa.Muro:
			return False, [-1,-1]
			
		else:
			return True, [jugador.posX, jugador.posY]		
		
		
	def cambiarElEstadoDelJuego(self, nuevoEstadoDelJuego):
		self.estadoDelJuego = nuevoEstadoDelJuego
		
	def buscarJugadorPorPosicion(self, posicion, peticion):
		indice = 0
		for jugador in self.jugadores:
			if jugador.posXActual == posicion[0] and jugador.posYActual == posicion[1] and jugador.tipoJugador != peticion:
				break;
			else:
				indice += 1
		
		return indice
		
	def reubicarJugador(nombreDelJugador):
		otro, indiceDelJugador = elJugadorExisteEnElJuego(nombreDelJugador)
		
		miJugador = jugadores[indiceDelJugador]
		jugadores.remove(miJugador)
		jugadores.insert(0, miJugador)
			
	#AgregarMapa
	#AgregarGalletas